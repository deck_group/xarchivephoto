from django.shortcuts import render
import base64
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework.decorators import api_view
from io import BytesIO
from PIL import Image
import os
import os.path

@api_view(['POST','GET'])
def personnel_image(request): 
    file = request.data['image']
    starter = file.find(',')
    image_data = file[starter+1:]
    image_data = bytes(image_data, encoding="ascii")
    im = Image.open(BytesIO(base64.b64decode(image_data)))
    dirname = os.path.dirname(__file__)
    path_image= os.path.join(dirname, "static"+'/personnel/')
    imagedir = path_image+request.data['imagename']
    if im.mode in ["RGBA", "P"]:
        im = im.convert("RGB")
        im.save(imagedir,format='JPEG', quality=95)
        pass
    else:
        im.save(imagedir)
        pass
    return JsonResponse({'message': '{}successfully!'}, status=200)

@api_view(['POST','GET'])
def aircraft_image(request): 
    file = request.data['image']
    starter = file.find(',')
    image_data = file[starter+1:]
    image_data = bytes(image_data, encoding="ascii")
    im = Image.open(BytesIO(base64.b64decode(image_data)))
    dirname = os.path.dirname(__file__)
    path_image= os.path.join(dirname, "static/personnel")
    imagedir = path_image+"/"+request.data['imagename']
    im.save(imagedir)
    return JsonResponse({'message': '{}successfully!'}, status=200)

@api_view(['POST','GET'])
def asset_image(request): 
    file = request.data['image']
    starter = file.find(',')
    image_data = file[starter+1:]
    image_data = bytes(image_data, encoding="ascii")
    im = Image.open(BytesIO(base64.b64decode(image_data)))

    orifile=request.data['oriimage']
    starterori = orifile.find(',')
    imageori_data = orifile[starterori+1:]
    imageori_data = bytes(imageori_data, encoding="ascii")
    imori = Image.open(BytesIO(base64.b64decode(imageori_data)))

    dirname = os.path.dirname(__file__)
    path_image= os.path.join(dirname,"static/asset")

    path_image_ori= os.path.join(dirname,"static/assetoriginal")
    imagedir = path_image+"/"+request.data['imagename']+".jpg"
    if im.mode in ["RGBA", "P"]:
        im = im.convert("RGB")
        im.save(imagedir,format='JPEG', quality=95)
        pass
    else:
        im.save(imagedir)
        pass 

    imagedir_ori = path_image_ori+"/"+request.data['oriimagename']+".jpg"

    if imori.mode in ["RGBA", "P"]:
        imori = imori.convert("RGB")
        imori.save(imagedir_ori,format='JPEG', quality=95)
        pass
    else:
        imori.save(imagedir_ori)
        pass 

    return JsonResponse({'message': '{}successfully!'}, status=200)